export const parseDateTime = (date) => {
  if (!date) return "Unknown date";
  return date.substring(0, 16).replace("T", " ");
};

export const parseDate = (date) => {
  if (!date) return "Unknown date";
  return date.substring(0, 10).replace("T", " ");
};
