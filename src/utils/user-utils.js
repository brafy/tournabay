export const getUserStatistics = (user, ruleset = "OSU") => {
  return user.statistics.find((s) => s.ruleset.toLowerCase() === ruleset.toLowerCase());
};
