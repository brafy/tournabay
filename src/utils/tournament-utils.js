export const isRegistrationPhase = (tournament) => {
  const now = new Date(Date.now());
  const regStartDate = new Date(Date.parse(tournament.registrationStartDate));
  const regEndDate = new Date(Date.parse(tournament.registrationEndDate));
  return now > regStartDate && now < regEndDate;
};

export const isStaffMember = (user, tournament) => {
  if (!user.isAuthenticated) return false;
  return tournament.staffMembers.some((staff) => staff.user.id === user?.id);
};

export const isMatchReferee = (user, match, tournament) => {
  if (!user.isAuthenticated) return false;
  const staffMember = tournament.staffMembers.find((staff) => staff.user.id === user?.id);
  if (!staffMember) return false;
  return match.referees.some((referee) => referee.id === staffMember.id);
};

export const isMatchCommentator = (user, match, tournament) => {
  if (!user.isAuthenticated) return false;
  const staffMember = tournament.staffMembers.find((staff) => staff.user.id === user?.id);
  if (!staffMember) return false;
  return match.commentators.some((commentator) => commentator.id === staffMember.id);
};

export const isMatchStreamer = (user, match, tournament) => {
  if (!user.isAuthenticated) return false;
  const staffMember = tournament.staffMembers.find((staff) => staff.user.id === user?.id);
  if (!staffMember) return false;
  return match.streamers.some((streamer) => streamer.id === staffMember.id);
};
