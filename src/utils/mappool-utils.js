export const modificationColors = (modification) => {
  switch (modification) {
    case "NM":
      return "#355ebe";
    case "HD":
      return "#ccb62b";
    case "HR":
      return "#b61c1c";
    case "DT":
      return "#2ad3d3";
    case "FM":
      return "#00ffa2";
    case "EZ":
      return "#28c523";
    case "HT":
      return "#851d94";
    case "FL":
      return "#a1a1a1";
    case "TB":
      return "#000000";
  }
};
