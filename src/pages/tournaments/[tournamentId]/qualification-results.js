import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import useOsuAuth from "../../../hooks/useOsuAuth";
import QualificationResults from "../../../components/qualification-results";

const TournamentStaffPage = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();
  const [isLoading, setIsLoading] = useState(true);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Qualification results</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        <QualificationResults tournament={tournament} />
      </Box>
    </Container>
  );
};

TournamentStaffPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentStaffPage;
