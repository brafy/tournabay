import { Avatar, Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import TournamentGuard from "../../../guards/TournamentGuard";

const TournamentPage = () => {
  const { tournament } = useTournament();
  const [isLoading, setIsLoading] = useState(true);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">{tournament.name}</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              A
            </Typography>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
            sx={{
              display: {
                md: "flex",
                xs: "none",
              },
              justifyContent: "center",
            }}
          >
            <Avatar
              src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/logo.jpg`}
              sx={{
                border: "3px solid #FFFFFF",
                height: 200,
                width: 200,
              }}
            />
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

TournamentPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentPage;
