import {
  Box,
  Button,
  Checkbox,
  Container,
  Divider,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useMemo, useRef, useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import dynamic from "next/dynamic";
import Slider from "@mui/material/Slider";
import * as React from "react";
import useOsuAuth from "../../../hooks/useOsuAuth";
import NotFound from "../../404";
import { QuillEditor } from "../../../components/quill-editor";
import { tournamentApi } from "../../../api/tournamentApi";
import { useRouter } from "next/router";
import { notifyOnError } from "../../../utils/error-response";
import toast from "react-hot-toast";
import { useDispatch } from "react-redux";
import { setTournament } from "../../../slices/tournament";
import { isRegistrationPhase } from "../../../utils/tournament-utils";

const TournamentRegistrationPage = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();
  const router = useRouter();
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [utcRange, setUtcRange] = useState([-2, 2]);
  const [discordTag, setDiscordTag] = useState(null);
  const [rulesCheckbox, setRulesCheckbox] = useState(false);
  const [discordCheckbox, setDiscordCheckbox] = useState(false);
  const quillRef = useRef();
  const ReactQuill = useMemo(() => dynamic(import("react-quill"), { ssr: false }), [tournament]);

  if (!isRegistrationPhase(tournament)) {
    return (
      <Container
        sx={{
          mt: 5,
          mb: 5,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            backgroundColor: "background.default",
            py: 6,
          }}
        >
          <Grid alignItems="center" container>
            <Grid item md={12} xs={12}>
              <Typography variant="h1">Registration phase ended!</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
                {tournament.name}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    );
  }

  if (!user.isAuthenticated) {
    return (
      <Container
        sx={{
          mt: 5,
          mb: 5,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            backgroundColor: "background.default",
            py: 6,
          }}
        >
          <Grid alignItems="center" container>
            <Grid item md={12} xs={12}>
              <Typography variant="h1">Log in to see registration form</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
                {tournament.name}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    );
  }

  const valuetext = (value) => {
    return `UTC${value > 1 && "+"}${value}`;
  };

  const handleUtcRangeChange = (event, newValue, activeThumb) => {
    if (!Array.isArray(newValue)) {
      return;
    }
    if (activeThumb === 0) {
      setUtcRange([Math.min(newValue[0], utcRange[1]), utcRange[1]]);
    } else {
      setUtcRange([utcRange[0], Math.max(newValue[1], utcRange[0])]);
    }
  };

  const isDiscordAccountLinked = () => {
    if (!user.isAuthenticated) return false;
    return !(user.discordData == null || user.discordData.length === 0);
  };

  const isRegisterButtonEnabled = () => {
    if (tournament.settings.requireDiscordAccount) {
      return rulesCheckbox && discordCheckbox && discordTag != null;
    }
    return rulesCheckbox && discordTag != null;
  };

  const handleRegisterButton = () => {
    if (tournament.settings.requireDiscordAccount && discordTag == null) {
      toast.error("Discord tag is required");
    }
    const toastId = toast.loading("Registering...");
    const body = {
      timezoneRange: utcRange,
      discordTag,
    };
    tournamentApi
      .register(tournament.id, body)
      .then((response) => {
        dispatch(setTournament(response.data));
        router.push(`/tournaments/${tournament.id}/participants`);
      })
      .catch((error) => notifyOnError(error))
      .finally(() => toast.dismiss(toastId));
  };

  if (tournament.teamFormat === "TEAM_VS" && tournament.settings.enableParticipantRegistration) {
    return (
      <Container
        sx={{
          mt: 5,
          mb: 5,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            backgroundColor: "background.default",
            py: 6,
          }}
        >
          <Grid alignItems="center" container>
            <Grid item md={6} xs={12}>
              <Typography variant="h1">Registration form</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
                {tournament.name}
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Divider />
        <Box
          sx={{
            backgroundColor: "background.default",
            minHeight: "100%",
            pt: 3,
          }}
        >
          <Box sx={{ my: 2 }}>
            <Typography sx={{ my: 1 }} variant="h6">
              UTC range
            </Typography>
            <Typography variant="body2">
              Select your UTC range. This will help organizers to adjust the schedule to your
              timezone.
            </Typography>
            <Box sx={{ width: "100%", mt: 1 }}>
              <Slider
                getAriaLabel={() => "UTC range"}
                value={utcRange}
                onChange={handleUtcRangeChange}
                valueLabelDisplay="auto"
                getAriaValueText={valuetext}
                disableSwap
                marks
                step={1}
                min={-12}
                max={12}
              />
            </Box>
          </Box>
          {!isDiscordAccountLinked() && tournament.settings.requireDiscordAccount && (
            <Box sx={{ my: 2 }}>
              <Typography sx={{ my: 1 }} variant="h6">
                Discord data
              </Typography>
              <Typography variant="body2">
                Tournament requires your basic information regarding your discord account.
              </Typography>
              <Box sx={{ width: "100%", mt: 1 }}>
                <TextField
                  fullWidth
                  helperText="i.e. John#2137"
                  label="Discord tag"
                  name="discordTag"
                  onChange={(event) => setDiscordTag(event.target.value)}
                  required
                  value={discordTag}
                  variant="outlined"
                />
              </Box>
            </Box>
          )}
          <Box sx={{ my: 2 }}>
            <Box sx={{ mb: 1 }}>
              <Typography sx={{ my: 1 }} variant="h6">
                Rules
              </Typography>
              <Typography variant="body2">Read tournament rules carefully</Typography>
            </Box>
            <Box sx={{ maxHeight: 500, overflow: "scroll" }}>
              <ReactQuill
                ref={quillRef.current}
                value={tournament.rules}
                readOnly
                theme={"bubble"}
              />
            </Box>
          </Box>
          <Box sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
            <Checkbox
              checked={rulesCheckbox}
              color="primary"
              onChange={(event) => setRulesCheckbox(event.target.checked)}
            />
            <Typography sx={{ my: 1 }} variant="body2">
              I&apos;ve read and agreed to the tournament rules
            </Typography>
          </Box>
          {tournament.settings.requireDiscordAccount && (
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
              <Checkbox
                checked={discordCheckbox}
                color="primary"
                onChange={(event) => setDiscordCheckbox(event.target.checked)}
              />
              <Typography sx={{ my: 1 }} variant="body2">
                I&apos;ve joined the tournament discord server
              </Typography>
            </Box>
          )}
          <Box sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
            <Button
              variant="contained"
              onClick={handleRegisterButton}
              disabled={!isRegisterButtonEnabled()}
            >
              Register
            </Button>
          </Box>
        </Box>
      </Container>
    );
  } else {
    return <NotFound />;
  }
};

TournamentRegistrationPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentRegistrationPage;
