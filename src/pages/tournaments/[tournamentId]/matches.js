import {
  Avatar,
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import ParticipantCard from "../../../components/participant-card";
import rca from "rainbow-colors-array";
import Scrollbar from "../../../components/scrollbar";
import { SeverityPill } from "../../../components/severity-pill";
import { UserCircle as UserCircleIcon } from "../../../icons/user-circle";
import { parseDateTime } from "../../../utils/date-time-utils";
import useOsuAuth from "../../../hooks/useOsuAuth";
import {
  isMatchCommentator,
  isMatchReferee,
  isMatchStreamer,
  isStaffMember,
} from "../../../utils/tournament-utils";
import axios from "axios";
import { ACCESS_TOKEN, API_URL } from "../../../constants/constants";
import toast from "react-hot-toast";
import { useDispatch } from "react-redux";
import { updateMatch } from "../../../slices/tournament";
import { notifyOnError } from "../../../utils/error-response";

const Matches = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  const handleStaffMemberSignIn = (match, action) => {
    setIsLoading(true);
    const toastId = toast.loading("Signing in...");
    axios
      .patch(
        `${API_URL}/match/staff-member-sign-in/match/${match.id}/tournament/${tournament.id}?action=${action}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
          },
        }
      )
      .then((response) => {
        dispatch(updateMatch(response.data));
        toast.success("Signed in successfully!");
      })
      .catch((error) => notifyOnError(error))
      .finally(() => {
        setIsLoading(false);
        toast.dismiss(toastId);
      });
  };

  const handleStaffMemberSignOut = (match, action) => {
    setIsLoading(true);
    const toastId = toast.loading("Signing out...");
    axios
      .patch(
        `${API_URL}/match/staff-member-sign-out/match/${match.id}/tournament/${tournament.id}?action=${action}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem(ACCESS_TOKEN)}`,
          },
        }
      )
      .then((response) => {
        dispatch(updateMatch(response.data));
        toast.success("Signed out successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => {
        setIsLoading(false);
        toast.dismiss(toastId);
      });
  };

  const teamVsMatches = () => {
    return tournament.matches.map((match) => {
      return (
        <TableRow key={match.id} hover>
          <TableCell align="left">
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "flex-start" }}>
              <Avatar
                src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${match.redTeam.id}/logo.jpg`}
                sx={{
                  height: 30,
                  width: 30,
                  mr: 1,
                }}
              />
              {match.redTeam.name}
            </Box>
          </TableCell>
          <TableCell align="right">
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "flex-end" }}>
              {match.blueTeam.name}{" "}
              <Avatar
                src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/team/${match.redTeam.id}/logo.jpg`}
                sx={{
                  height: 30,
                  width: 30,
                  ml: 1,
                }}
              />
            </Box>
          </TableCell>
          <TableCell>{parseDateTime(match.ldt)}</TableCell>
          <TableCell>
            {match.referees.map((referee) => (
              <Box key={referee.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={referee.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {referee.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchReferee(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "REFEREE_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchReferee(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "REFEREE_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>
            {match.commentators.map((commentator) => (
              <Box key={commentator.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={commentator.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {commentator.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchCommentator(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "COMMENTATOR_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchCommentator(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "COMMENTATOR_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>
            {match.streamers.map((streamer) => (
              <Box key={streamer.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={streamer.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {streamer.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchStreamer(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "STREAMER_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchStreamer(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "STREAMER_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>{match.isLive && <SeverityPill color="error">LIVE</SeverityPill>}</TableCell>
        </TableRow>
      );
    });
  };

  const participantVsMatches = () => {
    return tournament.matches.map((match) => {
      return (
        <TableRow key={match.id} hover>
          <TableCell align="left">
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "flex-start" }}>
              <Avatar
                src={`https://a.ppy.sh/${match.redParticipant.user.osuId}`}
                sx={{
                  height: 30,
                  width: 30,
                  mr: 1,
                }}
              />
              {match.redParticipant.user.username}
            </Box>
          </TableCell>
          <TableCell align="right">
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "flex-end" }}>
              {match.blueParticipant.user.username}{" "}
              <Avatar
                src={`https://a.ppy.sh/${match.blueParticipant.user.osuId}`}
                sx={{
                  height: 30,
                  width: 30,
                  ml: 1,
                }}
              />
            </Box>
          </TableCell>
          <TableCell>{parseDateTime(match.ldt)}</TableCell>
          <TableCell>
            {match.referees.map((referee) => (
              <Box key={referee.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={referee.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {referee.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchReferee(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "REFEREE_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchReferee(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "REFEREE_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>
            {match.commentators.map((commentator) => (
              <Box key={commentator.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={commentator.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {commentator.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchCommentator(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "COMMENTATOR_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchCommentator(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "COMMENTATOR_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>
            {match.streamers.map((streamer) => (
              <Box key={streamer.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
                <Avatar
                  src={streamer.user.avatarUrl}
                  sx={{
                    height: 30,
                    width: 30,
                    mr: 1,
                  }}
                />
                {streamer.user.username}
              </Box>
            ))}
            {isStaffMember(user, tournament) && !isMatchStreamer(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignIn(match, "STREAMER_SIGN_IN")}
                size="small"
              >
                Sign in
              </Button>
            )}
            {isMatchStreamer(user, match, tournament) && (
              <Button
                onClick={() => handleStaffMemberSignOut(match, "STREAMER_SIGN_OUT")}
                size="small"
                color="error"
              >
                Sign out
              </Button>
            )}
          </TableCell>
          <TableCell>{match.isLive && <SeverityPill color="error">LIVE</SeverityPill>}</TableCell>
        </TableRow>
      );
    });
  };

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Matches</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        <Scrollbar>
          <TableContainer>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell align="left">RED</TableCell>
                  <TableCell align="right">BLUE</TableCell>
                  <TableCell>Starts at</TableCell>
                  <TableCell>Referees</TableCell>
                  <TableCell>Commentators</TableCell>
                  <TableCell>Streamers</TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {tournament.teamFormat === "TEAM_VS" ? teamVsMatches() : participantVsMatches()}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>
      </Box>
    </Container>
  );
};

Matches.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default Matches;
