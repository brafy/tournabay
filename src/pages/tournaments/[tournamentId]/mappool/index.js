import * as React from "react";
import { Box, Container, Divider, Grid, Link, Typography } from "@mui/material";
import { MainTournamentLayout } from "../../../../components/main-tournament-layout";
import TournamentData from "../../../../guards/TournamentData";
import useTournament from "../../../../hooks/useTournament";
import { useEffect, useState } from "react";
import { mappoolApi } from "../../../../api/mappoolApi";
import { notifyOnError } from "../../../../utils/error-response";
import MappoolCard from "../../../../components/mappool-card";

const Mappool = () => {
  const { tournament } = useTournament();
  const [isLoading, setIsLoading] = useState(true);
  const [mappools, setMappools] = useState([]);

  useEffect(() => {
    mappoolApi
      .getPublishedMappools(tournament.id)
      .then((response) => {
        setMappools(response.data);
      })
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  }, []);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Mappools</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        {mappools.map((mappool) => (
          <React.Fragment key={mappool.id}>
            <MappoolCard mappool={mappool} tournament={tournament} />
          </React.Fragment>
        ))}
      </Box>
    </Container>
  );
};

Mappool.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default Mappool;
