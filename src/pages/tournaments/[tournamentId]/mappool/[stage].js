import { useRouter } from "next/router";
import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import * as React from "react";
import MappoolCard from "../../../../components/mappool-card";
import useTournament from "../../../../hooks/useTournament";
import TournamentData from "../../../../guards/TournamentData";
import TournamentGuard from "../../../../guards/TournamentGuard";
import { DashboardLayout } from "../../../../components/dashboard-layout";
import TournamentMappool from "../../../dashboard/tournament/[tournamentId]/mappool";
import Head from "next/head";
import { MainTournamentLayout } from "../../../../components/main-tournament-layout";
import { useEffect, useState } from "react";
import { mappoolApi } from "../../../../api/mappoolApi";
import { notifyOnError } from "../../../../utils/error-response";
import MappoolModificationRow from "../../../../components/mappool-modification-row";

const Mappool = () => {
  const { tournament } = useTournament();
  const router = useRouter();
  const { stage } = router.query;
  const [mappool, setMappool] = useState(null);

  useEffect(() => {
    mappoolApi
      .getMappoolByStage(tournament.id, stage)
      .then((response) => setMappool(response.data))
      .catch((error) => notifyOnError(error));
  }, []);

  return (
    <>
      <Head>
        <title>
          {stage} Mappool | {tournament.name}
        </title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 4,
        }}
      >
        <Container maxWidth={false}>
          <Container
            sx={{
              mt: 5,
              mb: 5,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Box
              sx={{
                backgroundColor: "background.default",
                py: 6,
              }}
            >
              <Grid alignItems="center" container>
                <Grid item md={6} xs={12}>
                  <Typography variant="h1">{stage} Mappool</Typography>
                  <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
                    {tournament.name}
                  </Typography>
                </Grid>
              </Grid>
            </Box>
            <Divider />
            <Box
              sx={{
                backgroundColor: "background.default",
                minHeight: "100%",
                pt: 3,
              }}
            >
              {mappool?.beatmapModifications.map((bm) => {
                if (bm.hidden || bm.beatmaps.length === 0) return undefined;
                return <MappoolModificationRow key={bm.id} beatmapModification={bm} />;
              })}
            </Box>
          </Container>
        </Container>
      </Box>
    </>
  );
};

Mappool.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default Mappool;
