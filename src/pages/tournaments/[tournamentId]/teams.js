import { Box, Button, Card, Container, Dialog, Divider, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import NotFound from "../../404";
import TeamCard from "../../../components/team-card";
import useOsuAuth from "../../../hooks/useOsuAuth";
import TeamManagementDialog from "../../../components/team-management-dialog";

const TeamsPage = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();
  const [isLoading, setIsLoading] = useState(true);
  const [isDialogOpen, setIsDialogOpen] = useState(false);

  if (tournament.teamFormat === "PLAYER_VS") return <NotFound />;

  const isTeamCaptain = () => {
    if (!user.isAuthenticated) return false;
    return tournament.teams.some((team) => team.captain.user.id === user?.id);
  };

  const getCaptainTeam = () => {
    if (!isTeamCaptain()) return null;
    return tournament.teams.find((team) => team.captain.user.id === user?.id);
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };

  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      {isTeamCaptain() && (
        <>
          <Card
            sx={{
              mt: 4,
              alignItems: "center",
              justifyContent: "space-between",
              backgroundColor: "success.light",
              color: "success.contrastText",
              display: "flex",
              flexDirection: {
                xs: "column",
                md: "row",
              },
              p: 4,
            }}
          >
            <Box>
              <Typography color="inherit" sx={{ mt: 2 }} variant="h4">
                You are a team captain!
              </Typography>
              <Typography color="inherit" sx={{ mt: 1 }} variant="subtitle2">
                Change team logo, banner, name, and more.
              </Typography>
            </Box>
            <Button onClick={handleDialogOpen} color="success" variant="contained">
              Manage team
            </Button>
          </Card>
          <Dialog fullWidth maxWidth="sm" onClose={handleDialogClose} open={isDialogOpen}>
            {isDialogOpen && (
              <TeamManagementDialog team={getCaptainTeam()} closeModal={handleDialogClose} />
            )}
          </Dialog>
        </>
      )}
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Teams</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        <Grid container spacing={2}>
          {tournament.teams.map((team, index) => (
            <Grid item key={team.id} lg={3} md={3} sm={6} xs={12}>
              <TeamCard team={team} tournament={tournament} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
};

TeamsPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TeamsPage;
