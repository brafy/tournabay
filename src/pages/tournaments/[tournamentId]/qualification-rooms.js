import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import QualificationRoomCard from "../../../components/qualification-room-card";
import useOsuAuth from "../../../hooks/useOsuAuth";

const TournamentStaffPage = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();
  const [isLoading, setIsLoading] = useState(true);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Qualification rooms</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        {tournament.qualificationRooms.length === 0 && (
          <Box
            sx={{ minHeight: 400, display: "flex", justifyContent: "center", alignItems: "center" }}
          >
            <Typography variant="h3">Qualification rooms are not released yet</Typography>
          </Box>
        )}
        <Grid container spacing={2}>
          {tournament.qualificationRooms.map((room) => (
            <Grid item key={room.id} lg={4} md={4} sm={6} xs={12}>
              <QualificationRoomCard room={room} user={user} tournament={tournament} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </Container>
  );
};

TournamentStaffPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentStaffPage;
