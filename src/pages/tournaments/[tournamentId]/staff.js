import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardMedia,
  Container,
  Divider,
  Grid,
  Link,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { MainTournamentLayout } from "../../../components/main-tournament-layout";
import TournamentData from "../../../guards/TournamentData";
import useTournament from "../../../hooks/useTournament";
import StaffMemberCard from "../../../components/staff-member-card";

const TournamentStaffPage = () => {
  const { tournament } = useTournament();
  const [isLoading, setIsLoading] = useState(true);

  return (
    <Container
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">Staff Members</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              {tournament.name}
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Divider />
      <Box
        sx={{
          backgroundColor: "background.default",
          minHeight: "100%",
          pt: 3,
        }}
      >
        {tournament.roles.map((role) => {
          if (role.isHidden) return undefined;
          const staffMembersWithRole = tournament.staffMembers.filter((staffMember) =>
            staffMember.tournamentRoles.some((staffRole) => staffRole.id === role.id)
          );
          if (staffMembersWithRole.length === 0) return undefined;
          return (
            <Box key={role.id} sx={{ mb: 4 }}>
              <Typography variant="h4">{role.name}</Typography>
              <Grid container spacing={2}>
                {staffMembersWithRole.map((staffMember) => (
                  <Grid item key={staffMember.id} lg={3} md={4} sm={6} xs={12}>
                    <StaffMemberCard staffMember={staffMember} />
                  </Grid>
                ))}
              </Grid>
            </Box>
          );
        })}
      </Box>
    </Container>
  );
};

TournamentStaffPage.getLayout = (page) => (
  <TournamentData>
    <MainTournamentLayout>{page}</MainTournamentLayout>
  </TournamentData>
);

export default TournamentStaffPage;
