import { Box, Container, Divider, Grid, Typography } from "@mui/material";
import { MainLayout } from "../components/main-layout";
import { tournamentApi } from "../api/tournamentApi";
import { notifyOnError } from "../utils/error-response";
import { useEffect, useState } from "react";
import TournamentCard from "../components/tournament-card";

const MyTournamentsPage = () => {
  const [tournaments, setTournaments] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    tournamentApi
      .myTournaments()
      .then((response) => setTournaments(response.data))
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  }, []);

  return (
    <Container
      maxWidth="md"
      sx={{
        mt: 5,
        mb: 5,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          backgroundColor: "background.default",
          py: 6,
        }}
      >
        <Grid alignItems="center" container spacing={3}>
          <Grid item md={6} xs={12}>
            <Typography variant="h1">My tournaments</Typography>
            <Typography color="textSecondary" sx={{ mt: 1 }} variant="body1">
              List of all tournaments you are associated with.
            </Typography>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
            sx={{
              display: {
                md: "flex",
                xs: "none",
              },
              justifyContent: "center",
            }}
          >
            <img alt="Components" src="/static/browse/hero.svg" />
          </Grid>
        </Grid>
      </Box>
      <Divider sx={{ my: 5 }} />
      <Box sx={{ display: "flex", flexWrap: "wrap" }}>
        {tournaments.map((tournament) => (
          <TournamentCard
            key={tournament.id}
            tournament={tournament}
            showFooter={false}
            navigateDashboard={true}
          />
        ))}
      </Box>
    </Container>
  );
};

MyTournamentsPage.getLayout = (page) => <MainLayout>{page}</MainLayout>;

export default MyTournamentsPage;
