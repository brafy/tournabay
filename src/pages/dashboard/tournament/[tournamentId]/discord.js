import TournamentData from "../../../../guards/TournamentData";
import TournamentGuard from "../../../../guards/TournamentGuard";
import { DashboardLayout } from "../../../../components/dashboard-layout";
import Head from "next/head";
import { Box, Button, Card, CardContent, Container, Typography } from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import RegistrationSettings from "../../../../components/dashboard/tournament/settings/registration-settings";
import useOsuAuth from "../../../../hooks/useOsuAuth";
import AddDiscordBot from "../../../../components/dashboard/tournament/discord/add-discord-bot";
import DiscordBotManagement from "../../../../components/dashboard/tournament/discord/discord-bot-management";

const DiscordPage = () => {
  const { tournament } = useTournament();
  const { user } = useOsuAuth();

  if (user?.discordData.length === 0) {
    return (
      <Container maxWidth={false}>
        <p>Tournament owner has to have discord account linked</p>
      </Container>
    );
  }

  const isUserAnOwner = tournament.owner.id === user.id;

  return (
    <>
      <Head>
        <title>Discord | {tournament.name}</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 4,
        }}
      >
        <Container maxWidth="md">
          <Typography sx={{ my: 2 }} variant="h4">
            Tournabay bot
          </Typography>
          {tournament.discordData === null && isUserAnOwner ? (
            <AddDiscordBot />
          ) : (
            <DiscordBotManagement />
          )}
        </Container>
      </Box>
    </>
  );
};

DiscordPage.getLayout = (page) => (
  <TournamentData>
    <TournamentGuard>
      <DashboardLayout>{page}</DashboardLayout>
    </TournamentGuard>
  </TournamentData>
);

export default DiscordPage;
