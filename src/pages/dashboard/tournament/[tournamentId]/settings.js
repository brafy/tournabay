import TournamentData from "../../../../guards/TournamentData";
import TournamentGuard from "../../../../guards/TournamentGuard";
import { DashboardLayout } from "../../../../components/dashboard-layout";
import Head from "next/head";
import { Box, Container, Divider, Tab, Tabs, Typography } from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import RegistrationSettings from "../../../../components/dashboard/tournament/settings/registration-settings";
import { useState } from "react";
import TournamentImages from "../../../../components/dashboard/tournament/settings/tournament-images";
import QualificationRoomSettings from "../../../../components/dashboard/tournament/settings/qualification-room-settings";
import TournamentLinksSettings from "../../../../components/dashboard/tournament/settings/tournament-links-settings";

const tabs = [
  { label: "Registration", value: "registration" },
  { label: "Images", value: "images" },
  { label: "Qualification rooms", value: "qualification-rooms" },
  { label: "Links", value: "links" },
];

const TournamentSettings = () => {
  const { tournament } = useTournament();
  const [currentTab, setCurrentTab] = useState("registration");

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  return (
    <>
      <Head>
        <title>Settings | {tournament.name}</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 4,
        }}
      >
        <Container maxWidth="md">
          <Typography sx={{ my: 2 }} variant="h4">
            Settings
          </Typography>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
            sx={{ mt: 3 }}
          >
            {tabs.map((tab) => (
              <Tab key={tab.value} label={tab.label} value={tab.value} />
            ))}
          </Tabs>
          <Divider sx={{ mb: 3 }} />
          {currentTab === "registration" && <RegistrationSettings />}
          {currentTab === "images" && <TournamentImages />}
          {currentTab === "qualification-rooms" && <QualificationRoomSettings />}
          {currentTab === "links" && <TournamentLinksSettings />}
        </Container>
      </Box>
    </>
  );
};

TournamentSettings.getLayout = (page) => (
  <TournamentData>
    <TournamentGuard>
      <DashboardLayout>{page}</DashboardLayout>
    </TournamentGuard>
  </TournamentData>
);

export default TournamentSettings;
