import { useEffect } from "react";
import NextLink from "next/link";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import { Box, Button, Drawer, Link, useMediaQuery } from "@mui/material";
import { styled } from "@mui/material/styles";
import UserAvatar from "./user-avatar";
import { OSU_AUTH_URL } from "../constants/constants";
import useOsuAuth from "../hooks/useOsuAuth";

const MainSidebarLink = styled(Link)(({ theme }) => ({
  borderRadius: theme.shape.borderRadius,
  display: "block",
  padding: theme.spacing(1.5),
  "&:hover": {
    backgroundColor: theme.palette.action.hover,
  },
}));

export const MainSidebar = (props) => {
  const { onClose, open } = props;
  const router = useRouter();
  const { user } = useOsuAuth();
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"));

  const handlePathChange = () => {
    if (open) {
      onClose?.();
    }
  };

  useEffect(
    handlePathChange,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [router.asPath]
  );

  return (
    <Drawer
      anchor="right"
      onClose={onClose}
      open={!lgUp && open}
      PaperProps={{ sx: { width: 256 } }}
      sx={{
        zIndex: (theme) => theme.zIndex.appBar + 100,
      }}
      variant="temporary"
    >
      <Box sx={{ p: 2 }}>
        <NextLink href="/tournament/new" passHref>
          <MainSidebarLink color="textSecondary" underline="none" variant="subtitle2">
            Create tournament
          </MainSidebarLink>
        </NextLink>
        <NextLink href="/tournaments" passHref>
          <MainSidebarLink color="textSecondary" underline="none" variant="subtitle2">
            Browse tournaments
          </MainSidebarLink>
        </NextLink>
        {user.isAuthenticated ? (
          <UserAvatar />
        ) : (
          <Button
            component="a"
            href={OSU_AUTH_URL}
            size="medium"
            sx={{ ml: 2 }}
            // target="_blank"
            variant="contained"
          >
            Login
          </Button>
        )}
      </Box>
    </Drawer>
  );
};

MainSidebar.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool,
};
