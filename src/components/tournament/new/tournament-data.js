import PropTypes from "prop-types";
import {
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { ArrowRight as ArrowRightIcon } from "../../../icons/arrow-right";
import { MobileDatePicker } from "@mui/lab";
import { useDispatch, useSelector } from "../../../store";
import {
  setRegistrationEndDate,
  setRegistrationStartDate,
  setTournamentEndDate,
  setTournamentInitials,
  setTournamentMaxStage,
  setTournamentName,
  setTournamentStartDate,
} from "../../../slices/tournamentWizard";
import { tournamentApi } from "../../../api/tournamentApi";
import { setTournament } from "../../../slices/tournament";

const stages = ["RO128", "RO64", "RO32", "RO16"];

export const TournamentData = (props) => {
  const { onBack, onNext, handleComplete, ...other } = props;
  const { tournament } = useSelector((state) => state.tournamentWizard);
  const dispatch = useDispatch();

  const handleRegistrationStartDateChange = (date) => {
    dispatch(setRegistrationStartDate(date));
    dispatch(setTournamentStartDate(date));
  };

  const handleRegistrationEndDateChange = (date) => {
    dispatch(setRegistrationEndDate(date));
  };

  const handleEndDateChange = (newValue) => {
    dispatch(setTournamentEndDate(newValue));
  };

  const handleNameChange = (name) => {
    dispatch(setTournamentName(name));
  };

  const handleInitialsChange = (name) => {
    dispatch(setTournamentInitials(name));
  };

  const handleMaxStageChange = (e) => {
    console.log(e.target.value);
    dispatch(setTournamentMaxStage(e.target.value));
  };

  const handleTournamentCreation = () => {
    tournamentApi
      .createTournament(tournament)
      .then((response) => {
        dispatch(setTournament(response.data));
        handleComplete();
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div {...other}>
      <Typography variant="h6">Tournament details</Typography>
      <Box sx={{ mt: 3 }}>
        <TextField
          sx={{ mb: 3 }}
          fullWidth
          label="Tournament name"
          name="tournamentName"
          placeholder="e.g Gotta Aim Fast"
          onChange={(e) => handleNameChange(e.target.value)}
          value={tournament.name}
        />
        <TextField
          sx={{ mb: 3 }}
          fullWidth
          label="Initials"
          name="tournamentInitials"
          placeholder="e.g GAF"
          onChange={(e) => handleInitialsChange(e.target.value)}
          value={tournament.initials}
        />
        <Typography sx={{ mt: 3 }} variant="subtitle1">
          Registration period
        </Typography>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            mt: 3,
          }}
        >
          <MobileDatePicker
            label="Registration start date"
            inputFormat="dd/MM/yyyy"
            value={tournament.registrationStartDate}
            onChange={handleRegistrationStartDateChange}
            renderInput={(inputProps) => <TextField {...inputProps} />}
          />
          <Box sx={{ ml: 2 }}>
            <MobileDatePicker
              label="Registration end date"
              inputFormat="dd/MM/yyyy"
              value={tournament.registrationEndDate}
              onChange={handleRegistrationEndDateChange}
              renderInput={(inputProps) => <TextField {...inputProps} />}
            />
          </Box>
        </Box>
        <Typography sx={{ mt: 3 }} variant="subtitle1">
          Tournament period
        </Typography>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            mt: 3,
          }}
        >
          <MobileDatePicker
            label="Start Date"
            inputFormat="dd/MM/yyyy"
            value={tournament.startDate}
            onChange={handleRegistrationStartDateChange}
            renderInput={(inputProps) => <TextField {...inputProps} />}
          />
          <Box sx={{ ml: 2 }}>
            <MobileDatePicker
              label="End Date"
              inputFormat="dd/MM/yyyy"
              value={tournament.endDate}
              onChange={handleEndDateChange}
              renderInput={(inputProps) => <TextField {...inputProps} />}
            />
          </Box>
        </Box>
        <Typography sx={{ mt: 3 }} variant="subtitle1">
          Max stage
        </Typography>
        <Box sx={{ mt: 3 }}>
          <FormControl size="medium" sx={{ minWidth: 150 }}>
            <InputLabel id="base-team-size">Max stage</InputLabel>
            <Select
              labelId="base-team-size"
              id="base-team-size"
              value={tournament.maxStage}
              label="Base team size"
              onChange={handleMaxStageChange}
            >
              {stages.map((stage) => (
                <MenuItem key={stage} value={stage}>
                  {stage}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Box>
      </Box>
      <Box sx={{ mt: 3 }}>
        <Button
          endIcon={<ArrowRightIcon fontSize="small" />}
          onClick={handleTournamentCreation}
          variant="contained"
        >
          Continue
        </Button>
        <Button onClick={onBack} sx={{ ml: 2 }}>
          Back
        </Button>
      </Box>
    </div>
  );
};

TournamentData.propTypes = {
  onBack: PropTypes.func,
  onNext: PropTypes.func,
  handleComplete: PropTypes.func,
};
