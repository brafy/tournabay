import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { mappoolApi } from "../api/mappoolApi";
import { qualificationsApi } from "../api/qualificationsApi";
import { notifyOnError } from "../utils/error-response";
import { setQualificationResultsDto } from "../slices/tournament";
import Scrollbar from "./scrollbar";
import { useDispatch } from "react-redux";

const QualificationResults = (props) => {
  const { tournament } = props;
  const [beatmaps, setBeatmaps] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    mappoolApi
      .getQualifierBeatmaps(tournament.id)
      .then((response) => {
        setBeatmaps(response.data);
        qualificationsApi
          .getQualificationResults(tournament.id)
          .then((response) => {
            dispatch(setQualificationResultsDto(response.data));
          })
          .catch((error) => notifyOnError(error));
      })
      .catch((error) => notifyOnError(error));
  }, []);

  return (
    <Box>
      {beatmaps.length === 0 && (
        <Box
          sx={{ minHeight: 400, display: "flex", justifyContent: "center", alignItems: "center" }}
        >
          <Typography variant="h3">Qualifier mappool or results not found!</Typography>
        </Box>
      )}
      {beatmaps.length > 0 && (
        <Scrollbar>
          <TableContainer>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>Team</TableCell>
                  {beatmaps?.map((beatmap) => {
                    return (
                      <TableCell
                        key={beatmap.id}
                        sx={{
                          "&:before": {
                            content: "''",
                            position: "absolute",
                            top: 0,
                            left: 0,
                            width: "100%",
                            height: "100%",
                            backgroundImage: `url(${beatmap.normalCover})`,
                            backgroundSize: "cover",
                            backgroundPosition: "center",
                            opacity: 0.4,
                          },
                        }}
                      >
                        <Typography
                          variant="body1"
                          sx={{
                            position: "relative",
                            textShadow: "2px 3px 5px rgba(0,0,0,0.5)",
                            color: "white !important",
                          }}
                        >
                          {beatmap.modification + beatmap.position}
                        </Typography>
                      </TableCell>
                    );
                  })}
                  <TableCell>Total</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {beatmaps?.length > 0 &&
                  tournament?.qualificationResultsDto?.map((result) => (
                    <QualificationResultRow
                      key={result.qualificationPoints}
                      result={result}
                      beatmaps={beatmaps}
                      tournament={tournament}
                    />
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>
      )}
    </Box>
  );
};

const QualificationResultRow = (props) => {
  const { result, beatmaps, tournament } = props;

  const teamResults = () => (
    <>
      <TableRow key={result.id} hover>
        <TableCell>{result.team.name}</TableCell>
        {beatmaps?.map((beatmap) => {
          const s = result.scores.find((s) => beatmap?.id === s.beatmap.id);
          return (
            <>
              <Tooltip
                arrow
                title={
                  <Box>
                    <p>x̄Score: {s?.averageScore}</p>
                    <p>x̄Acc: {s?.averageAccuracy * 100}%</p>
                    <p>Points: {s?.qualificationPoints}</p>
                  </Box>
                }
              >
                <TableCell key={s?.id}>
                  {tournament.scoreType === "ACCURACY"
                    ? (s?.averageAccuracy * 100).toFixed(2) + "%"
                    : s?.averageScore}{" "}
                  <Typography
                    sx={{
                      display: "inline",
                      color: s?.qualificationPoints === 1 ? "red" : "aqua",
                    }}
                    variant="body2"
                  >
                    ({s?.qualificationPoints.toFixed(2)})
                  </Typography>
                </TableCell>
              </Tooltip>
            </>
          );
        })}
        <TableCell sx={{ color: "red", fontWeight: "bold" }}>
          {result.qualificationPoints.toFixed(2)}
        </TableCell>
      </TableRow>
    </>
  );

  const playerResults = () => (
    <>
      <TableRow key={result.id} hover>
        <TableCell>{result.participant.user.username}</TableCell>
        {beatmaps?.map((beatmap) => {
          const s = result.scores.find((s) => beatmap?.id === s.beatmap.id);
          return (
            <>
              <Tooltip
                arrow
                title={
                  <Box>
                    <p>x̄Score: {s?.averageScore}</p>
                    <p>x̄Acc: {s?.averageAccuracy * 100}%</p>
                    <p>Points: {s?.qualificationPoints}</p>
                  </Box>
                }
              >
                <TableCell key={s?.id}>
                  {tournament.scoreType === "ACCURACY"
                    ? (s?.averageAccuracy * 100).toFixed(2) + "%"
                    : s?.averageScore}{" "}
                  <Typography
                    sx={{
                      display: "inline",
                      color: s?.qualificationPoints === 1 ? "red" : "aqua",
                    }}
                    variant="body2"
                  >
                    ({s?.qualificationPoints.toFixed(2)})
                  </Typography>
                </TableCell>
              </Tooltip>
            </>
          );
        })}
        <TableCell sx={{ color: "red", fontWeight: "bold" }}>
          {result.qualificationPoints.toFixed(2)}
        </TableCell>
      </TableRow>
    </>
  );

  return tournament.teamFormat === "TEAM_VS" ? teamResults() : playerResults();
};

QualificationResults.propTypes = {
  tournament: PropTypes.object.isRequired,
};

QualificationResultRow.propTypes = {
  result: PropTypes.object.isRequired,
  tournament: PropTypes.object.isRequired,
  beatmaps: PropTypes.array.isRequired,
};

export default QualificationResults;
