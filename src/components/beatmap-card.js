import { Badge, Box, Button, Card, CardMedia, IconButton, Link, Typography } from "@mui/material";
import { Bell as BellIcon } from "../icons/bell";
import PanoramaFishEyeIcon from "@mui/icons-material/PanoramaFishEye";
import CircleIcon from "@mui/icons-material/Circle";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import BubbleChartIcon from "@mui/icons-material/BubbleChart";
import { Star as StarIcon } from "../icons/star";
import { Clock as ClockIcon } from "../icons/clock";
import { useEffect, useState } from "react";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { SeverityPill } from "./severity-pill";

const BeatmapCard = (props) => {
  const { beatmap, index, mod } = props;
  const [modAttributes, setModAttributes] = useState();

  useEffect(() => {
    let attributes = beatmap.stats.find((stat) => stat.modification === mod);
    if (attributes === undefined) {
      attributes = beatmap.stats.find((stat) => stat.modification === "NM");
    }
    setModAttributes(attributes);
  }, [beatmap, mod]);

  const minutes = Math.floor(modAttributes?.length / 60);
  const seconds = modAttributes?.length - minutes * 60;

  return (
    <Box
      sx={{
        outline: "none",
        py: 1,
        mx: 1,
      }}
    >
      <Card
        sx={{
          "&:hover": {
            backgroundColor: "background.default",
          },
          display: "flex",
        }}
        variant={"outlined"}
      >
        <CardMedia
          image={beatmap.normalCover}
          sx={{ height: "auto", width: "30%" }}
          component="a"
          href={`https://osu.ppy.sh/beatmapsets/${beatmap.beatmapsetId}#osu/${beatmap.beatmapId}`}
          target="_blank"
        />
        <Box
          sx={{
            width: "100%",
            position: "relative",
            p: 2,
            "& > div > div > p": {
              m: 0,
              fontSize: 10,
            },
          }}
        >
          <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "flex-start" }}>
            <Box>
              <span>{beatmap.title}</span>
              <p>[{beatmap.version}]</p>
              <p>mapped by {beatmap.creator}</p>
            </Box>
          </Box>
          <Box
            sx={{
              mt: 1,
              display: "flex",
              flexWrap: "wrap",
              "& *": {
                mr: 0.2,
                mb: 0.2,
              },
            }}
          >
            <SeverityPill color="primary">
              <PanoramaFishEyeIcon fontSize="small" /> AR:{" "}
              {parseFloat(modAttributes?.ar.toFixed(2))}
            </SeverityPill>
            <SeverityPill color="primary">
              <CircleIcon fontSize="small" /> CS: {parseFloat(modAttributes?.cs.toFixed(2))}
            </SeverityPill>
            <SeverityPill color="primary">
              <FavoriteBorderIcon fontSize="small" /> HP: {parseFloat(modAttributes?.hp.toFixed(2))}
            </SeverityPill>
            <SeverityPill color="primary">
              <BubbleChartIcon fontSize="small" /> AC:{" "}
              {parseFloat(modAttributes?.accuracy.toFixed(2))}
            </SeverityPill>
            <SeverityPill color="warning">
              <StarIcon fontSize="small" /> {parseFloat(modAttributes?.stars.toFixed(2))}
            </SeverityPill>
            <SeverityPill color="warning">
              <ClockIcon fontSize="small" /> {minutes}:{seconds < 10 ? "0" + seconds : seconds}
            </SeverityPill>
          </Box>
          <Typography
            sx={(theme) => ({
              position: "absolute",
              bottom: -100,
              right: 20,
              fontSize: 200,
              opacity: 0.03,
              fontWeight: "bold",
              color: theme.palette.primary.main,
            })}
            variant="body2"
          >
            {mod !== "TB" && mod + index}
          </Typography>
        </Box>
      </Card>
    </Box>
  );
};

export default BeatmapCard;
