import { Box, IconButton, Paper, Typography } from "@mui/material";
import { DotsHorizontal } from "../icons/dots-horizontal";
import PropTypes from "prop-types";
import BeatmapCard from "./beatmap-card";

const MappoolModificationRow = (props) => {
  const { beatmapModification } = props;

  return (
    <Paper
      sx={{
        display: "flex",
        flexDirection: "column",
        maxHeight: "100%",
        mx: 1,
        mb: 2,
        overflowX: "hidden",
        overflowY: "hidden",
      }}
    >
      <Typography sx={{ pt: 5, pl: 5 }} color="inherit" onClick={() => {}} variant="h4">
        {beatmapModification.modification}
      </Typography>
      <Box
        sx={{
          alignItems: "center",
          display: "flex",
          px: 2,
          py: 1,
        }}
      >
        <Box
          sx={{
            flexGrow: 1,
            minHeight: 80,
            overflowY: "auto",
            px: 2,
            py: 1,
          }}
        >
          {beatmapModification.beatmaps.map((beatmap, index) => (
            <BeatmapCard
              key={beatmap.id}
              beatmap={beatmap}
              index={index + 1}
              mod={beatmapModification.modification}
            />
          ))}
        </Box>
      </Box>
    </Paper>
  );
};

MappoolModificationRow.propTypes = {
  beatmapModification: PropTypes.object.isRequired,
};

export default MappoolModificationRow;
