import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import PropTypes from "prop-types";
import useTournament from "../../../../hooks/useTournament";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData, setSettings, setTournamentSettings } from "../../../../slices/tournament";
import { tournamentSettingsApi } from "../../../../api/tournamentSettingsApi";
import { useDispatch } from "react-redux";

const QualificationRoomSettings = (props) => {
  const { tournament } = useTournament();
  const [settings, setSettings] = useState(tournament.settings);
  const dispatch = useDispatch();

  const handleSettingInputChange = (e) => {
    const { name, value } = e.target;
    setSettings((prevSettings) => ({
      ...prevSettings,
      [name]: value,
    }));
  };

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating tournament settings");
    tournamentSettingsApi
      .updateTournamentSettings(tournament.id, settings)
      .then((response) => {
        dispatch(setTournamentSettings(response.data));
        toast.success("Tournament settings updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Qualification rooms details</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Room limit</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Maximum teams / participants in qualification room.
              </Typography>
              <TextField
                fullWidth
                label="Room limit"
                placeholder="i.e. 4"
                size="small"
                name="qualificationRoomLimit"
                value={settings.qualificationRoomLimit}
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Staff members limit</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Maximum staff members in qualification room.
              </Typography>
              <TextField
                fullWidth
                label="Staff members limit"
                placeholder="i.e. 1"
                size="small"
                value={settings.qualificationRoomStaffLimit}
                name="qualificationRoomStaffLimit"
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Sign in limit</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Closes the room registration option after {settings.qualificationRoomSignInLimit}{" "}
                hours.
              </Typography>
              <TextField
                fullWidth
                label="Registration close hours"
                placeholder="i.e. 8"
                size="small"
                value={settings.qualificationRoomSignInLimit}
                name="qualificationRoomStaffLimit"
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default QualificationRoomSettings;
