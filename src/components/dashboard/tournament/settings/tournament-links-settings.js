import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import PropTypes from "prop-types";
import useTournament from "../../../../hooks/useTournament";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData, setSettings, setTournamentSettings } from "../../../../slices/tournament";
import { tournamentSettingsApi } from "../../../../api/tournamentSettingsApi";
import { useDispatch } from "react-redux";

const TournamentLinksSettings = (props) => {
  const { tournament } = useTournament();
  const [settings, setSettings] = useState(tournament.settings);
  const dispatch = useDispatch();

  const handleSettingInputChange = (e) => {
    const { name, value } = e.target;
    setSettings((prevSettings) => ({
      ...prevSettings,
      [name]: value,
    }));
  };

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating tournament settings");
    tournamentSettingsApi
      .updateTournamentSettings(tournament.id, settings)
      .then((response) => {
        dispatch(setTournamentSettings(response.data));
        toast.success("Tournament settings updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Tournament links</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Osu! thread</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Link to your tournament&apos;s thread on the osu! forums.
              </Typography>
              <TextField
                fullWidth
                label="Osu! thread URL"
                placeholder="i.e. 4"
                size="small"
                name="osuThreadUrl"
                value={settings.osuThreadUrl}
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Discord server invitation</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Link to your tournament&apos;s Discord server.
              </Typography>
              <TextField
                fullWidth
                label="Discord server invitation URL"
                placeholder="i.e. 1"
                size="small"
                value={settings.discordServerUrl}
                name="discordServerUrl"
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Bracket</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Link to your tournament&apos;s bracket. i.e. Challonge
              </Typography>
              <TextField
                fullWidth
                label="Bracket URL"
                placeholder="i.e. 8"
                size="small"
                value={settings.bracketUrl}
                name="bracketUrl"
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default TournamentLinksSettings;
