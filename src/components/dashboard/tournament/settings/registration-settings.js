import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import { useState } from "react";
import PropTypes from "prop-types";
import useTournament from "../../../../hooks/useTournament";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData, setSettings, setTournamentSettings } from "../../../../slices/tournament";
import { tournamentSettingsApi } from "../../../../api/tournamentSettingsApi";
import { useDispatch } from "react-redux";

const RegistrationSettings = (props) => {
  const { tournament } = useTournament();
  const [settings, setSettings] = useState(tournament.settings);
  const dispatch = useDispatch();

  const handleSettingInputChange = (e) => {
    const { name, value } = e.target;
    setSettings((prevSettings) => ({
      ...prevSettings,
      [name]: value,
    }));
  };

  const handleSettingsSwitchChange = (e) => {
    const { name, checked } = e.target;
    setSettings((prevSettings) => ({
      ...prevSettings,
      [name]: checked,
    }));
  };

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating tournament settings");
    tournamentSettingsApi
      .updateTournamentSettings(tournament.id, settings)
      .then((response) => {
        dispatch(setTournamentSettings(response.data));
        toast.success("Tournament settings updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Registration</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Enable team registration</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Enables team registration for the tournament
                </Typography>
              </div>
              <Switch
                name="enableTeamRegistration"
                defaultChecked={settings.enableTeamRegistration}
                value={settings.enableTeamRegistration}
                onChange={handleSettingsSwitchChange}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Enable participants registration</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Enables participants registration for the tournament. This will allow users to
                  register as participants for the tournament. Used for auctioned tournaments if the
                  tournament is TEAM_VS_TEAM.
                </Typography>
              </div>
              <Switch
                name="enableParticipantRegistration"
                defaultChecked={settings.enableParticipantRegistration}
                value={settings.enableParticipantRegistration}
                onChange={handleSettingsSwitchChange}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Allow staff members to register</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Allows staff members to register for the tournament.
                </Typography>
              </div>
              <Switch
                name="allowStaffMembersToRegister"
                defaultChecked={settings.allowStaffMembersToRegister}
                value={settings.allowStaffMembersToRegister}
                onChange={handleSettingsSwitchChange}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Require discord account</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Requires users to verify their discord account before registering for the
                  tournament.
                </Typography>
              </div>
              <Switch
                name="requireDiscordAccount"
                defaultChecked={settings.requireDiscordAccount}
                value={settings.requireDiscordAccount}
                onChange={handleSettingsSwitchChange}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Allow players to join as staff members</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Enables players to join as staff members for the tournament.
                </Typography>
              </div>
              <Switch defaultChecked={false} disabled={true} />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Open rank</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Allows everyone to register for the tournament regardless of rank.
                </Typography>
              </div>
              <Switch
                name="openRank"
                defaultChecked={settings.openRank}
                value={settings.openRank}
                onChange={handleSettingsSwitchChange}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Base team size</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Minimum team size for the tournament.
              </Typography>
              <TextField
                fullWidth
                label="Base team size"
                placeholder="i.e. 4"
                size="small"
                name="baseTeamSize"
                value={settings.baseTeamSize}
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Max team size</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Maximum team size for the tournament.
              </Typography>
              <TextField
                fullWidth
                label="Max team size"
                placeholder="i.e. 8"
                size="small"
                value={settings.maxTeamSize}
                name="maxTeamSize"
                onChange={handleSettingInputChange}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default RegistrationSettings;
