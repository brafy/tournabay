import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  Link,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import useTournament from "../../../../hooks/useTournament";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData, setSettings, setTournamentSettings } from "../../../../slices/tournament";
import { tournamentSettingsApi } from "../../../../api/tournamentSettingsApi";
import { useDispatch } from "react-redux";
import { FileDropzone } from "../../../file-dropzone";
import axios from "axios";
import { ACCESS_TOKEN, API_URL, BASE_API_URL } from "../../../../constants/constants";
import DashboardTournamentCard from "../../../dashboard-tournament-card";

const TournamentImages = () => {
  const { tournament } = useTournament();
  const [logoFile, setLogoFile] = useState([]);
  const [logoFileUrl, setLogoFileUrl] = useState(
    `https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/logo.jpg`
  );
  const [tempLogoFileUrl, setTempLogoFileUrl] = useState(null);

  const [bannerFile, setBannerFile] = useState([]);
  const [bannerFileUrl, setBannerFileUrl] = useState(
    `https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/banner.jpg`
  );
  const [tempBannerFileUrl, setTempBannerFileUrl] = useState(null);

  const handleLogoDrop = (newFiles) => {
    setLogoFile([...newFiles]);
    setTempLogoFileUrl(URL.createObjectURL(newFiles[0]));
  };

  const handleLogoRemove = (file) => {
    setLogoFile([]);
  };

  const handleLogoRemoveAll = () => {
    setLogoFile([]);
  };

  const handleBannerDrop = (newFiles) => {
    setBannerFile([...newFiles]);
    setTempBannerFileUrl(URL.createObjectURL(newFiles[0]));
  };

  const handleBannerRemove = (file) => {
    setBannerFile([]);
  };

  const handleBannerRemoveAll = () => {
    setBannerFile([]);
  };

  const onLogoUpload = () => {
    const toastLoadingId = toast.loading("Uploading logo");
    let formData = new FormData();

    formData.append("file", logoFile[0]);

    axios
      .put(`${API_URL}/s3/upload/logo/tournament/${tournament.id}?fileType=LOGO`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
        },
      })
      .then((response) => {
        setTempLogoFileUrl(null);
        setLogoFile([]);
        toast.success("Logo uploaded!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  const onBannerUpload = () => {
    const toastLoadingId = toast.loading("Uploading banner");
    let formData = new FormData();

    formData.append("file", bannerFile[0]);

    axios
      .put(`${API_URL}/s3/upload/logo/tournament/${tournament.id}?fileType=BANNER`, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: "Bearer " + localStorage.getItem(ACCESS_TOKEN),
        },
      })
      .then((response) => {
        setTempBannerFileUrl(null);
        setBannerFile([]);
        toast.success("Banner uploaded!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Card
          sx={{
            width: "100%",
            boxShadow: "none",
            backgroundPosition: "center",
            backgroundSize: "cover",
            backgroundImage: tempBannerFileUrl
              ? `url('${tempBannerFileUrl}')`
              : `url('${bannerFileUrl}?${Date.now()}')`,
          }}
        >
          {/*<CardMedia*/}
          {/*  image={tempBannerFileUrl ? tempBannerFileUrl : `${bannerFileUrl}?${Date.now()}`}*/}
          {/*  sx={{ height: 200, width: "auto" }}*/}
          {/*/>*/}
          <CardContent sx={{ p: "5px !important", background: `rgba(0,0,0,0.4)` }}>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                my: 2,
                // mt: "-50px",
              }}
            >
              <Avatar
                alt="Applicant"
                src={tempLogoFileUrl ? tempLogoFileUrl : `${logoFileUrl}?${Date.now()}`}
                sx={{
                  border: "3px solid #FFFFFF",
                  height: 80,
                  width: 80,
                }}
              />
            </Box>
            <Link
              align="center"
              color="textPrimary"
              sx={{ display: "block" }}
              underline="none"
              variant="h6"
            >
              {tournament.name}
            </Link>
            <Typography align="center" variant="body2" color="textSecondary">
              {tournament.initials}
            </Typography>
          </CardContent>
        </Card>
      </Box>
      <Card sx={{ mt: 3 }}>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={4} xs={12}>
              <Typography variant="h6">Logo</Typography>
              <Typography color="textSecondary" variant="body2" sx={{ mt: 1 }}>
                Upload a logo for the tournament. Accepted extensions .jpg, .png, .jpeg. Max file
                size 5MB.
              </Typography>
            </Grid>
            <Grid item md={8} xs={12}>
              <FileDropzone
                accept="image/jpeg, image/png, image/jpg"
                maxFiles={1}
                files={logoFile}
                onDrop={handleLogoDrop}
                onRemove={handleLogoRemove}
                onRemoveAll={handleLogoRemoveAll}
                onUpload={onLogoUpload}
                // in B
                // max 5MB
                maxSize={5000000}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <Card sx={{ mt: 3 }}>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={4} xs={12}>
              <Typography variant="h6">Banner</Typography>
              <Typography color="textSecondary" variant="body2" sx={{ mt: 1 }}>
                Upload a banner for the tournament. Accepted extensions .jpg, .png, .jpeg. Max file
                size 5MB.
              </Typography>
            </Grid>
            <Grid item md={8} xs={12}>
              <FileDropzone
                accept="image/jpeg, image/png, image/jpg"
                maxFiles={1}
                files={bannerFile}
                onDrop={handleBannerDrop}
                onRemove={handleBannerRemove}
                onRemoveAll={handleBannerRemoveAll}
                onUpload={onBannerUpload}
                // in B
                // max 5MB
                maxSize={5000000}
              />
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
};

export default TournamentImages;
