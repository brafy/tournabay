import { Box, Button, Dialog, Divider, TextField } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import PropTypes from "prop-types";
import { useState } from "react";
import { useDispatch } from "../../../../store";
import QualificationRoomCard from "./QualificationRoomCard";
import CreateQualificationRoomDialog from "./CreateQualificationRoomDialog";
import toast from "react-hot-toast";
import { tournamentSettingsApi } from "../../../../api/tournamentSettingsApi";
import { setTournamentSettings } from "../../../../slices/tournament";

const QualificationRoomsGrid = (props) => {
  const { tournament } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [settings, setSettings] = useState(tournament.settings);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const dispatch = useDispatch();

  const handleSettingInputChange = (e) => {
    const { name, value } = e.target;
    setSettings((prevSettings) => ({
      ...prevSettings,
      [name]: value,
    }));
  };

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating tournament settings");
    tournamentSettingsApi
      .updateTournamentSettings(tournament.id, settings)
      .then((response) => {
        dispatch(setTournamentSettings(response.data));
        toast.success("Tournament settings updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };

  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };

  return (
    <Box>
      <h2>Qualification rooms settings</h2>
      <Box sx={{ display: "flex", flexWrap: "wrap" }}>
        <TextField
          sx={{ mr: 2 }}
          type="number"
          label="Room limit"
          placeholder="i.e. 4"
          value={settings.qualificationRoomLimit}
          onChange={handleSettingInputChange}
          name="qualificationRoomLimit"
          variant="outlined"
        />
        <TextField
          sx={{ mr: 2 }}
          type="number"
          label="Staff members limit"
          placeholder="i.e. 1"
          variant="outlined"
          value={settings.qualificationRoomStaffLimit}
          name="qualificationRoomStaffLimit"
          onChange={handleSettingInputChange}
        />
        <TextField
          sx={{ mr: 2 }}
          type="number"
          label="Registration close hours"
          variant="outlined"
          placeholder="i.e. 8"
          value={settings.qualificationRoomSignInLimit}
          name="qualificationRoomStaffLimit"
          onChange={handleSettingInputChange}
        />
        <Button variant="outlined" onClick={handleSaveButton}>
          Save
        </Button>
      </Box>
      <Divider sx={{ my: 2 }} />
      <Box sx={{ display: "flex", flexWrap: "wrap" }}>
        {tournament.qualificationRooms.map((room) => (
          <QualificationRoomCard key={room.id} room={room} tournament={tournament} />
        ))}
        <Button
          startIcon={<AddIcon />}
          onClick={handleDialogOpen}
          sx={(theme) => {
            return {
              p: 2,
              pt: 0,
              m: 1,
              minHeight: 200,
              border: "2px dashed " + theme.palette.primary.main,
              width: "calc(100% / 4 - 16px)",
              [theme.breakpoints.down("lg")]: {
                width: "calc(100% / 3 - 16px)",
              },
              [theme.breakpoints.down("md")]: {
                width: "calc(100% / 2 - 16px)",
              },
              [theme.breakpoints.down("sm")]: {
                width: "100%",
              },
            };
          }}
        >
          Create room
        </Button>
        <Dialog fullWidth maxWidth="sm" onClose={handleDialogClose} open={isDialogOpen}>
          {isDialogOpen && <CreateQualificationRoomDialog closeModal={handleDialogClose} />}
        </Dialog>
      </Box>
    </Box>
  );
};

QualificationRoomsGrid.propTypes = {
  tournament: PropTypes.object.isRequired,
};

export default QualificationRoomsGrid;
