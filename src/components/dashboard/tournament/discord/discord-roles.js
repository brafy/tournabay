import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  InputAdornment,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useDispatch } from "react-redux";
import { useState } from "react";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData } from "../../../../slices/tournament";

const DiscordRoles = (props) => {
  const { url } = props;
  const { tournament } = useTournament();
  const dispatch = useDispatch();

  const [playerRoleId, setPlayerRoleId] = useState(tournament.discordData.playerRoleId);
  const [captainRoleId, setCaptainRoleId] = useState(tournament.discordData.captainRoleId);

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating discord data");
    const body = {
      playerRoleId,
      captainRoleId,
    };
    discordApi
      .updateTournamentDiscordData(tournament.id, body, url)
      .then((response) => {
        dispatch(setDiscordData(response.data));
        toast.success("Discord details updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Roles management</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Captain role ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                The role ID of the captain role.
              </Typography>
              <TextField
                fullWidth
                label="Captain role ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={captainRoleId}
                onChange={(event) => setCaptainRoleId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Player role ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                The role ID of the player role.
              </Typography>
              <TextField
                fullWidth
                label="Verified role ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={playerRoleId}
                onChange={(event) => setPlayerRoleId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default DiscordRoles;
