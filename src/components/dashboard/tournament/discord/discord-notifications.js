import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  InputAdornment,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useDispatch } from "react-redux";
import { useState } from "react";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData } from "../../../../slices/tournament";

const DiscordNotifications = (props) => {
  const { url } = props;
  const { tournament } = useTournament();
  const dispatch = useDispatch();

  const [notifyReschedulement, setNotifyReschedulement] = useState(
    tournament.discordData.notifyReschedulement
  );
  const [sendLogs, setSendLogs] = useState(tournament.discordData.sendLogs);

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating discord data");
    const body = {
      notifyReschedulement,
      sendLogs,
    };
    discordApi
      .updateTournamentDiscordData(tournament.id, body, url)
      .then((response) => {
        dispatch(setDiscordData(response.data));
        toast.success("Discord details updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Notification details</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Notify reschedulements</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  If a match is rescheduled, the teams and participants and staff associated with
                  the match will be notified via Discord in reschedule channel specified in Channels
                  tab.
                </Typography>
              </div>
              <Switch
                value={notifyReschedulement}
                defaultChecked={notifyReschedulement}
                onChange={(e) => setNotifyReschedulement(e.target.checked)}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Send logs</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Log all the actions performed by the staff or teams or participants in the log
                  channel specified in Channels tab.
                </Typography>
              </div>
              <Switch
                value={sendLogs}
                defaultChecked={sendLogs}
                onChange={(e) => setSendLogs(e.target.checked)}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default DiscordNotifications;
