import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  InputAdornment,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useState } from "react";
import { discordApi } from "../../../../api/discordApi";
import { useDispatch } from "react-redux";
import { setDiscordData } from "../../../../slices/tournament";
import toast from "react-hot-toast";

const DiscordVerification = (props) => {
  const { url } = props;
  const { tournament } = useTournament();
  const [enableVerification, setEnableVerification] = useState(
    tournament.discordData.enableVerification
  );
  const [verifiedRoleId, setVerifiedRoleId] = useState(tournament.discordData.verifiedRoleId);
  const dispatch = useDispatch();

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating discord data");
    const body = {
      enableVerification,
      verifiedRoleId,
    };
    discordApi
      .updateTournamentDiscordData(tournament.id, body, url)
      .then((response) => {
        dispatch(setDiscordData(response.data));
        toast.success("Discord details updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Verification details</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <div>
                <Typography variant="subtitle1">Enable verification</Typography>
                <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                  Requires users to link their Discord account to their account on the website to
                  see the abyss of your discord server.
                </Typography>
              </div>
              <Switch
                defaultChecked={enableVerification}
                value={enableVerification}
                onChange={(e) => setEnableVerification(e.target.checked)}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Verified role ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                If verification is enabled, this role will be given to the user on the Discord
                server.
              </Typography>
              <TextField
                fullWidth
                label="Verified role ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={verifiedRoleId}
                onChange={(e) => setVerifiedRoleId(e.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default DiscordVerification;
