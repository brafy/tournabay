import { Box, Card, CardContent, Divider, Tab, Tabs, Typography } from "@mui/material";
import { useState } from "react";
import DiscordVerification from "./discord-verification";
import DiscordMembers from "./discord-members";
import DiscordRoles from "./discord-roles";
import DiscordChannels from "./discord-channels";
import DiscordNotifications from "./discord-notifications";

const tabs = [
  { label: "Verification", value: "verification", url: "verification" },
  { label: "Members", value: "members" },
  { label: "Roles", value: "roles", url: "basic-roles" },
  { label: "Channels", value: "channels", url: "channels" },
  { label: "Notifications", value: "notifications", url: "notifications" },
];

const DiscordBotManagement = () => {
  const [currentTab, setCurrentTab] = useState("verification");

  const tabUrl = tabs.find((tab) => tab.value === currentTab)?.url;

  const handleTabsChange = (event, value) => {
    setCurrentTab(value);
  };

  return (
    <>
      <Tabs
        indicatorColor="primary"
        onChange={handleTabsChange}
        scrollButtons="auto"
        textColor="primary"
        value={currentTab}
        variant="scrollable"
        sx={{ mt: 3 }}
      >
        {tabs.map((tab) => (
          <Tab key={tab.value} label={tab.label} value={tab.value} />
        ))}
      </Tabs>
      <Divider sx={{ mb: 3 }} />
      {currentTab === "verification" && <DiscordVerification url={tabUrl} />}
      {currentTab === "members" && <DiscordMembers />}
      {currentTab === "roles" && <DiscordRoles url={tabUrl} />}
      {currentTab === "channels" && <DiscordChannels url={tabUrl} />}
      {currentTab === "notifications" && <DiscordNotifications url={tabUrl} />}
    </>
  );
};

export default DiscordBotManagement;
