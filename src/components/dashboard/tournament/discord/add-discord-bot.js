import { Box, Button, Card, CardContent, TextField, Typography } from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useState } from "react";
import { useRouter } from "next/router";
import { discordApi } from "../../../../api/discordApi";

const AddDiscordBot = () => {
  const { tournament } = useTournament();
  const [guildId, setGuildId] = useState(null);
  const router = useRouter();

  const handleGuildIdChange = (e) => {
    setGuildId(e.target.value);
  };

  const handleBotInvitation = () => {
    discordApi.registerGuildId(tournament.id, guildId).then((response) => {
      router.push(response.data);
    });
  };

  return (
    <Card>
      <CardContent>
        <Typography variant="h6">Invite the bot to your discord server</Typography>
        <p>Enter the ID of the discord server you want to add the bot to</p>
        <TextField
          sx={{ my: 1 }}
          fullWidth
          value={guildId}
          onChange={handleGuildIdChange}
          placeholder="Guild ID"
        />
        <Button onClick={handleBotInvitation} sx={{ my: 1 }} variant="outlined">
          Invite bot
        </Button>
      </CardContent>
    </Card>
  );
};

export default AddDiscordBot;
