import {
  Box,
  Card,
  CardContent,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Scrollbar from "../../../scrollbar";
import { discordApi } from "../../../../api/discordApi";
import useTournament from "../../../../hooks/useTournament";
import { useEffect, useState } from "react";

const DiscordMembers = () => {
  const { tournament } = useTournament();
  const [members, setMembers] = useState([]);
  const [verifiedMembers, setVerifiedMembers] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [showVerified, setShowVerified] = useState(false);

  useEffect(() => {
    discordApi
      .getDiscordServerMembers(tournament.id)
      .then((response) => {
        setMembers(response.data);
        setVerifiedMembers(response.data.filter((member) => member.verified));
      })
      .finally(() => setIsLoading(false));
  }, []);

  const getMembersByFilter = () => {
    if (showVerified) {
      return verifiedMembers;
    }
    return members;
  };

  return (
    <Card>
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            justifyContent: "space-between",
            mb: 3,
          }}
        >
          <div>
            <Typography variant="subtitle1">Show only verified members</Typography>
          </div>
          <Switch value={showVerified} onChange={(e) => setShowVerified(e.target.checked)} />
        </Box>
        <Scrollbar>
          <TableContainer>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell>Nickname</TableCell>
                  <TableCell>Ef. nickname</TableCell>
                  <TableCell>ID</TableCell>
                  <TableCell>Is verified?</TableCell>
                  <TableCell align="right">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {getMembersByFilter().map((member) => (
                  <TableRow key={member.id}>
                    <TableCell>{member.nickname}</TableCell>
                    <TableCell>{member.effectiveName}</TableCell>
                    <TableCell>{member.id}</TableCell>
                    <TableCell>{member.verified ? "Yes" : "No"}</TableCell>
                    <TableCell align="right">TODO</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Scrollbar>
      </CardContent>
    </Card>
  );
};

export default DiscordMembers;
