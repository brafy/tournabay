import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  InputAdornment,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import useTournament from "../../../../hooks/useTournament";
import { useDispatch } from "react-redux";
import { useState } from "react";
import toast from "react-hot-toast";
import { discordApi } from "../../../../api/discordApi";
import { setDiscordData } from "../../../../slices/tournament";

const DiscordChannels = (props) => {
  const { url } = props;
  const { tournament } = useTournament();
  const dispatch = useDispatch();

  const [announcementChannelId, setAnnouncementChannelId] = useState(
    tournament.discordData.announcementChannelId
  );
  const [staffAnnouncementChannelId, setStaffAnnouncementChannelId] = useState(
    tournament.discordData.staffAnnouncementChannelId
  );
  const [rescheduleChannelId, setRescheduleChannelId] = useState(
    tournament.discordData.rescheduleChannelId
  );
  const [logsChannelId, setLogsChannelId] = useState(tournament.discordData.logsChannelId);

  const handleSaveButton = () => {
    const toastLoadingId = toast.loading("Updating discord data");
    const body = {
      announcementChannelId,
      staffAnnouncementChannelId,
      rescheduleChannelId,
      logsChannelId,
    };
    discordApi
      .updateTournamentDiscordData(tournament.id, body, url)
      .then((response) => {
        dispatch(setDiscordData(response.data));
        toast.success("Discord details updated successfully!");
      })
      .catch((error) => console.error(error))
      .finally(() => toast.remove(toastLoadingId));
  };

  return (
    <Card>
      <CardContent>
        <Grid container spacing={3}>
          <Grid item md={4} xs={12}>
            <Typography variant="h6">Server channels</Typography>
          </Grid>
          <Grid item md={8} xs={12}>
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Announcement channel ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Channel where all members can see announcements.
              </Typography>
              <TextField
                fullWidth
                label="Announcement channel ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={announcementChannelId}
                onChange={(event) => setAnnouncementChannelId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Staff announcement channel ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Channel where only staff can see announcements.
              </Typography>
              <TextField
                fullWidth
                label="Staff announcement channel ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={staffAnnouncementChannelId}
                onChange={(event) => setStaffAnnouncementChannelId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Reschedule channel ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Channel where bot will announce any match reschedules.
              </Typography>
              <TextField
                fullWidth
                label="Reschedule channel ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={rescheduleChannelId}
                onChange={(event) => setRescheduleChannelId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box
              sx={{
                alignItems: "center",
                display: "flex-column",
                justifyContent: "space-between",
                mb: 3,
              }}
            >
              <Typography variant="subtitle1">Logs channel ID</Typography>
              <Typography color="textSecondary" sx={{ mt: 1 }} variant="body2">
                Channel where bot will log all actions regarding this tournament.
              </Typography>
              <TextField
                fullWidth
                label="Logs channel ID"
                placeholder="i.e. 889085263656157187"
                size="small"
                value={logsChannelId}
                onChange={(event) => setLogsChannelId(event.target.value)}
                sx={{
                  flexGrow: 1,
                  mt: 3,
                }}
              />
            </Box>
            <Divider
              sx={{
                mt: 3,
                mb: 3,
              }}
            />
            <Box sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="outlined" onClick={handleSaveButton}>
                Save
              </Button>
            </Box>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default DiscordChannels;
