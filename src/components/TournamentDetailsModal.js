import PropTypes from "prop-types";
import { Box, Card, CardMedia, CircularProgress } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { notifyOnError } from "../utils/error-response";
import { tournamentApi } from "../api/tournamentApi";
import Iframe from "react-iframe";

const TournamentDetailsModal = (props) => {
  const { tournament } = props;
  const [bbcode, setBbcode] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const threadRef = useRef(null);

  useEffect(() => {
    tournamentApi
      .bbCode(tournament.id)
      .then((response) => {
        threadRef.current.innerHTML = response.data;
        // setBbcode(response.data);
      })
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  }, []);

  return (
    <Box>
      <Card>
        <CardMedia
          image={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/banner.jpg`}
          sx={{ height: 300 }}
        />
        <Box sx={{ p: 2 }}>
          <div ref={threadRef}></div>
        </Box>
      </Card>
    </Box>
  );
};

TournamentDetailsModal.propTypes = {
  closeModal: PropTypes.func.isRequired,
};

export default TournamentDetailsModal;
