import { Box, Card, Container, Grid, Link, Typography } from "@mui/material";

export const HomeCards = (props) => (
  <Box
    sx={{
      backgroundColor: "background.paper",
      py: 15,
    }}
    {...props}
  >
    <Container maxWidth="lg">
      <Typography align="center" sx={{ pb: 6 }} variant="h3">
        Tournament management made easy
      </Typography>
      <Grid container spacing={3}>
        <Grid item md={6} xs={12}>
          <Card
            sx={{
              height: "100%",
              p: 3,
              position: "relative",
            }}
            variant="outlined"
          >
            <Typography sx={{ color: "textPrimary" }} variant="h5">
              Tournaments
            </Typography>
            <Typography
              sx={{
                color: "textPrimary",
                py: 2,
              }}
              variant="body2"
            >
              Browse through all the tournaments that are currently running.
            </Typography>
            <Link href="/tournaments" color="textPrimary" underline="always" variant="body1">
              Browse tournaments
            </Link>
          </Card>
        </Grid>
        <Grid item md={6} xs={12}>
          <Card
            sx={{
              height: "100%",
              p: 3,
              position: "relative",
            }}
            variant="outlined"
          >
            <Typography sx={{ color: "textPrimary" }} variant="h5">
              Try it yourself
            </Typography>
            <Typography
              sx={{
                color: "textPrimary",
                py: 2,
              }}
              variant="body2"
            >
              Create your own tournament and see how it works.
            </Typography>
            <Link href="/tournament/new" color="textPrimary" underline="always" variant="body1">
              Create a tournament
            </Link>
          </Card>
        </Grid>
      </Grid>
    </Container>
  </Box>
);
