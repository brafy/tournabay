import { Avatar, Box, Button, Card, CardContent, Divider, Typography } from "@mui/material";
import { parseDateTime } from "../utils/date-time-utils";
import { qualificationsApi } from "../api/qualificationsApi";
import { notifyOnError } from "../utils/error-response";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { setQualificationRooms } from "../slices/tournament";

const QualificationRoomCard = (props) => {
  const { room, user, tournament } = props;
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const isTeamVs = tournament.teamFormat === "TEAM_VS";

  const handleSignInButton = () => {
    setIsLoading(true);
    qualificationsApi
      .signIn(tournament.id, room.id)
      .then((response) => {
        dispatch(setQualificationRooms(response.data));
      })
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  };

  const handleStaffMemberSignInButton = () => {
    setIsLoading(true);
    qualificationsApi
      .staffMemberSignIn(tournament.id, room.id)
      .then((response) => {
        dispatch(setQualificationRooms(response.data));
      })
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  };

  const handleStaffMemberSignOutButton = () => {
    setIsLoading(true);
    qualificationsApi
      .staffMemberSignOut(tournament.id, room.id)
      .then((response) => {
        dispatch(setQualificationRooms(response.data));
      })
      .catch((error) => notifyOnError(error))
      .finally(() => setIsLoading(false));
  };

  const canSignIn = () => {
    if (isTeamVs) {
      const participant = tournament.participants.find((p) => p.user?.id === user?.id);
      const team = tournament.teams.find(
        (t) =>
          t.captain.user.id === participant?.user?.id ||
          t.participants.some((p) => p.user.id === participant?.user?.id)
      );

      return participant && team && team?.captain?.user?.id === user?.id && !isSignedIn();
    }
    return false;
  };

  const isSignedIn = () => {
    const participant = getUserParticipant();
    if (isTeamVs) {
      const team = tournament.teams.find(
        (t) =>
          t.captain.user.id === participant?.user?.id ||
          t.participants.some((p) => p.user.id === participant?.user?.id)
      );

      return room.teams.some((t) => t.id === team?.id);
    }
    return room.participants.some((p) => p.id === participant?.id);
  };

  const isSignedInAsStaffMember = () => {
    return room.staffMembers.some((s) => s.user.id === user?.id);
  };

  const getCurrentRoom = () => {
    const participant = getUserParticipant();
    if (isTeamVs) {
      const team = tournament.teams.find(
        (t) =>
          t.captain.user.id === participant?.user?.id ||
          t.participants.some((p) => p.user.id === participant?.user?.id)
      );
      return tournament.qualificationRooms.find((r) => r.teams.some((t) => t.id === team?.id));
    }
    return tournament.qualificationRooms.find((r) =>
      r.participants.some((p) => p.id === participant?.id)
    );
  };

  const getUserParticipation = () => {
    const participant = getUserParticipant();
    if (isTeamVs) {
      return tournament.teams.find(
        (t) =>
          t.captain.user.id === participant?.user?.id ||
          t.participants.some((p) => p.user.id === participant?.user?.id)
      );
    }
    return participant;
  };

  const getUserParticipant = () => {
    return tournament.participants.find((p) => p.user?.id === user?.id);
  };

  const isStaffMember = () => {
    return tournament.staffMembers.some((s) => s.user.id === user?.id);
  };

  const teams = () => {
    return room.teams.map((team) => (
      <Box key={team.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
        <Avatar
          alt={team.name}
          sx={{ height: 30, width: 30, mr: 1 }}
          src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/teams/${team.id}/logo.jpg`}
        />
        <Typography align="center" variant="body2">
          {team.name}
        </Typography>
      </Box>
    ));
  };

  const participants = () => {
    return room.participants.map((participant) => (
      <Box key={participant.id} sx={{ display: "flex", alignItems: "center", my: 1 }}>
        <Avatar sx={{ height: 30, width: 30, mr: 1 }} src={participant.user.avatarUrl} />
        <Typography align="center" variant="body2">
          {participant.user.username}
        </Typography>
      </Box>
    ));
  };

  const getBoxShadow = () => {
    if (isSignedIn()) {
      return "rgba(0, 0, 0, 0.16) 0px 1px 4px, rgb(0, 100, 0) 0px 0px 0px 3px";
    } else if (isSignedInAsStaffMember()) {
      return "rgba(0, 0, 0, 0.16) 0px 1px 4px, rgb(255, 80, 0) 0px 0px 0px 3px";
    }
    return undefined;
  };

  return (
    <Card
      sx={{
        boxShadow: getBoxShadow(),
      }}
    >
      <CardContent sx={{ pt: 0 }}>
        <Box sx={{ pt: 2, display: "flex", justifyContent: "space-between", alignItems: "center" }}>
          <Typography align="center" variant="h6">
            ROOM {room.symbol}
          </Typography>
          {canSignIn() && (
            <Button onClick={handleSignInButton} disabled={isLoading}>
              Sign in
            </Button>
          )}
        </Box>
        <Typography align="left" variant="body2">
          {parseDateTime(room.startDate)}
        </Typography>
        <Divider sx={{ my: 2 }} />
        <Box sx={{ minHeight: 150 }}>{isTeamVs ? teams() : participants()}</Box>
        <Divider sx={{ my: 2 }} />
        {room.staffMembers.map((staffMember) => (
          <Box
            key={staffMember.id}
            sx={{ display: "flex", alignItems: "center", justifyContent: "center", mt: 1 }}
          >
            <Avatar
              alt={staffMember.user.username}
              sx={{ height: 30, width: 30, mr: 1 }}
              src={staffMember.user.avatarUrl}
            />
            <Typography align="center" variant="body2">
              {staffMember.user.username}
            </Typography>
          </Box>
        ))}
        {isStaffMember() && (
          <Box sx={{ display: "flex", alignItems: "center", justifyContent: "center", mt: 1 }}>
            {isSignedInAsStaffMember() ? (
              <Button onClick={handleStaffMemberSignOutButton} disabled={isLoading} color="error">
                Sign out
              </Button>
            ) : (
              <Button onClick={handleStaffMemberSignInButton} disabled={isLoading}>
                Sign in as referee
              </Button>
            )}
          </Box>
        )}
      </CardContent>
    </Card>
  );
};

export default QualificationRoomCard;
