import PropTypes from "prop-types";
import NextLink from "next/link";
import { AppBar, Avatar, Box, Button, Container, IconButton, Link, Toolbar } from "@mui/material";
import { Menu as MenuIcon } from "../icons/menu";
import { Logo } from "./logo";
import { DISCORD_AUTH_URL, OSU_AUTH_URL } from "../constants/constants";
import useOsuAuth from "../hooks/useOsuAuth";
import UserAvatar from "./user-avatar";
import { discordApi } from "../api/discordApi";
import Router from "next/router";
import useTournament from "../hooks/useTournament";
import { isRegistrationPhase } from "../utils/tournament-utils";

export const MainTournamentNavbar = (props) => {
  const { onOpenSidebar } = props;
  const { user } = useOsuAuth();
  const { tournament } = useTournament();

  return (
    <AppBar
      elevation={0}
      sx={{
        backgroundColor: "background.paper",
        borderBottomColor: "divider",
        borderBottomStyle: "solid",
        borderBottomWidth: 1,
        color: "text.secondary",
      }}
    >
      <Container maxWidth="lg">
        <Toolbar disableGutters sx={{ minHeight: 64 }}>
          <NextLink href={`/tournaments/${tournament.id}`} passHref>
            <a>
              <Avatar
                src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/logo.jpg`}
                alt={tournament.name}
                sx={{
                  height: 50,
                  width: 50,
                }}
              />
            </a>
          </NextLink>
          <Box sx={{ flexGrow: 1 }} />
          <IconButton
            color="inherit"
            onClick={onOpenSidebar}
            sx={{
              display: {
                md: "none",
              },
            }}
          >
            <MenuIcon fontSize="small" />
          </IconButton>
          <Box
            sx={{
              alignItems: "center",
              display: {
                md: "flex",
                xs: "none",
              },
            }}
          >
            {isRegistrationPhase(tournament) && (
              <NextLink href={`/tournaments/${tournament.id}/registration`} passHref>
                <Link color="textSecondary" underline="none" variant="subtitle2">
                  Registration
                </Link>
              </NextLink>
            )}
            <NextLink href={`/tournaments/${tournament.id}/rules`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Rules
              </Link>
            </NextLink>
            <NextLink href={`/tournaments/${tournament.id}/staff`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Staff
              </Link>
            </NextLink>
            <NextLink href={`/tournaments/${tournament.id}/participants`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Participants
              </Link>
            </NextLink>
            {tournament.teamFormat === "TEAM_VS" && (
              <NextLink href={`/tournaments/${tournament.id}/teams`} passHref>
                <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                  Teams
                </Link>
              </NextLink>
            )}
            <NextLink href={`/tournaments/${tournament.id}/qualification-rooms`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Qualification rooms
              </Link>
            </NextLink>
            <NextLink href={`/tournaments/${tournament.id}/qualification-results`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Qualification results
              </Link>
            </NextLink>
            <NextLink href={`/tournaments/${tournament.id}/matches`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Matches
              </Link>
            </NextLink>
            <NextLink href={`/tournaments/${tournament.id}/mappool`} passHref>
              <Link color="textSecondary" sx={{ ml: 2 }} underline="none" variant="subtitle2">
                Mappool
              </Link>
            </NextLink>
            {user.isAuthenticated ? (
              <UserAvatar />
            ) : (
              <Button
                component="a"
                href={OSU_AUTH_URL}
                size="medium"
                sx={{ ml: 2 }}
                // target="_blank"
                variant="contained"
              >
                Login
              </Button>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

MainTournamentNavbar.propTypes = {
  onOpenSidebar: PropTypes.func,
};
