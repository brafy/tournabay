import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardMedia,
  Dialog,
  Divider,
  Grid,
  IconButton,
  Link,
  Tooltip,
  Typography,
} from "@mui/material";
import { formatDistanceToNowStrict } from "date-fns";
import { parseDate, parseDateTime } from "../utils/date-time-utils";
import { Users as UsersIcon } from "../icons/users";
import { red } from "@mui/material/colors";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import InfoIcon from "@mui/icons-material/Info";
import useOsuAuth from "../hooks/useOsuAuth";
import { tournamentApi } from "../api/tournamentApi";
import { notifyOnError } from "../utils/error-response";
import { useRouter } from "next/router";
import { useState } from "react";
import EditGroupDialog from "./dashboard/tournament/groups/EditGroupDialog";
import TournamentDetailsModal from "./TournamentDetailsModal";
import EditIcon from "@mui/icons-material/Edit";
import { OsuIcon } from "../icons/osu-icon";
import { DiscordIcon } from "../icons/discord-icon";
import LanIcon from "@mui/icons-material/Lan";

const TournamentCard = (props) => {
  const { tournament, showFooter = true, navigateDashboard = false } = props;
  const { user } = useOsuAuth();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  const now = new Date(Date.now());
  const regStartDate = new Date(Date.parse(tournament.registrationStartDate));
  const regEndDate = new Date(Date.parse(tournament.registrationEndDate));
  const endDate = new Date(Date.parse(tournament.endDate));

  const handleRegisterButtonClick = () => {
    router.push(`/tournaments/${tournament.id}/registration`);
  };

  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };

  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };

  const getTournamentStatus = () => {
    if (now < regStartDate) {
      return (
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
          }}
        >
          <Box
            sx={{
              alignItems: "center",
              display: "flex",
            }}
          >
            <FiberManualRecordIcon sx={{ color: "grey", fontSize: 10 }} />
            {"  "}
            <Typography color="textSecondary" variant="subtitle2" sx={{ ml: 1, color: "grey" }}>
              Announced
            </Typography>
          </Box>
        </Box>
      );
    } else if (now > regStartDate && now < regEndDate) {
      return (
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
          }}
        >
          <Box
            sx={{
              alignItems: "center",
              display: "flex",
            }}
          >
            <FiberManualRecordIcon sx={{ color: "green", fontSize: 10 }} />
            {"  "}
            <Typography color="textSecondary" variant="subtitle2" sx={{ ml: 1, color: "green" }}>
              Registration open
            </Typography>
          </Box>
        </Box>
      );
    }
  };

  const registerButton = () => {
    if (!user.isAuthenticated) {
      return (
        <Tooltip title="Log in to register">
          <IconButton sx={{ color: red["600"] }}>
            <Button color="primary" size="small" variant="contained" disabled>
              Register
            </Button>
          </IconButton>
        </Tooltip>
      );
    } else if (tournament.participants.some((participant) => participant.user.id === user.id)) {
      return (
        <Typography color="textSecondary" variant="body2">
          Already registered
        </Typography>
      );
    } else if (now > regStartDate && now < regEndDate) {
      return (
        <Button
          color="primary"
          size="small"
          variant="contained"
          disabled={isLoading}
          onClick={handleRegisterButtonClick}
        >
          Register
        </Button>
      );
    }
  };

  return (
    <>
      <Card
        sx={(theme) => {
          return {
            minHeight: 200,
            width: "calc(100% / 2 - 16px)",
            [theme.breakpoints.down("lg")]: {
              width: "calc(100% / 3 - 16px)",
            },
            [theme.breakpoints.down("md")]: {
              width: "calc(100% / 2 - 16px)",
            },
            [theme.breakpoints.down("sm")]: {
              width: "100%",
            },
            m: 1,
          };
        }}
      >
        <CardMedia
          image={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/banner.jpg`}
          sx={{ height: 80 }}
        />
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            mb: 1,
            mt: "-45px",
          }}
        >
          <Avatar
            alt="Applicant"
            src={`https://tournabay.s3.amazonaws.com/tournament/${tournament.id}/logo.jpg`}
            sx={{
              border: "3px solid #FFFFFF",
              height: 100,
              width: 100,
            }}
          />
        </Box>
        <CardContent sx={{ pt: 0 }}>
          <Box
            sx={{
              alignItems: "center",
              display: "flex",
              mt: 2,
            }}
          >
            <Avatar src={tournament.owner.avatarUrl} />
            <Box sx={{ ml: 2 }}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  flexWrap: "wrap",
                }}
              >
                <Link
                  color="textPrimary"
                  variant="h6"
                  href={
                    navigateDashboard
                      ? `/dashboard/tournament/${tournament.id}`
                      : `/tournaments/${tournament.id}`
                  }
                  sx={{ mr: 1 }}
                >
                  {tournament.name}
                </Link>
                {getTournamentStatus()}
              </Box>
              <Typography color="textSecondary" variant="body2">
                by{" "}
                <Link
                  color="textPrimary"
                  variant="subtitle2"
                  target="_blank"
                  href={`https://osu.ppy.sh/users/${tournament.owner.osuId}`}
                >
                  {tournament.owner.username}
                </Link>
              </Typography>
            </Box>
          </Box>
          <Box
            sx={{
              px: 3,
              py: 2,
            }}
          >
            <Grid alignItems="center" container justifyContent="space-between" spacing={3}>
              <Grid item>
                <Typography variant="subtitle2">
                  {tournament.settings.openRank ? "Open" : "Closed"} Rank
                </Typography>
                <Typography color="textSecondary" variant="body2">
                  Rank limit
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="subtitle2">{tournament.teamFormat}</Typography>
                <Typography color="textSecondary" variant="body2">
                  Team format
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="subtitle2">{tournament.gameMode}</Typography>
                <Typography color="textSecondary" variant="body2">
                  Mode
                </Typography>
              </Grid>
            </Grid>
          </Box>
          {showFooter && (
            <>
              <Divider />
              <Box
                sx={{
                  alignItems: "center",
                  display: "flex",
                  justifyContent: "space-between",
                  ml: 2,
                  mt: 2,
                }}
              >
                <Box
                  sx={{
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <UsersIcon fontSize="small" />
                  <Typography color="textSecondary" sx={{ ml: 1 }} variant="subtitle2">
                    {tournament.participants.length}
                  </Typography>
                </Box>
                {registerButton()}
              </Box>
            </>
          )}
          {tournament.settings.osuThreadUrl != null ||
            tournament.settings.discordServerUrl != null ||
            (tournament.settings.bracketUrl != null && <Divider sx={{ mt: 2 }} />)}
          <Divider sx={{ my: 2 }} />
          <Box sx={{ display: "flex", alignItems: "center", justifyContent: "flex-end" }}>
            {tournament.settings.osuThreadUrl && (
              <IconButton
                color="inherit"
                edge="end"
                sx={{ mr: 1 }}
                target={"_blank"}
                href={tournament.settings.osuThreadUrl}
              >
                <OsuIcon fontSize="large" />
              </IconButton>
            )}
            {tournament.settings.discordServerUrl && (
              <IconButton
                color="inherit"
                edge="end"
                target={"_blank"}
                sx={{ mr: 1 }}
                href={tournament.settings.osuThreadUrl}
              >
                <DiscordIcon fontSize="large" />
              </IconButton>
            )}
            {tournament.settings.bracketUrl && (
              <IconButton
                color="inherit"
                edge="end"
                target={"_blank"}
                href={tournament.settings.osuThreadUrl}
              >
                <LanIcon fontSize="large" />
              </IconButton>
            )}
          </Box>
        </CardContent>
      </Card>
      {/*<Dialog fullWidth maxWidth="md" onClose={handleDialogClose} open={isDialogOpen}>*/}
      {/*  {isDialogOpen && (*/}
      {/*    <TournamentDetailsModal tournament={tournament} closeModal={handleDialogClose} />*/}
      {/*  )}*/}
      {/*</Dialog>*/}
    </>
  );
};

export default TournamentCard;
