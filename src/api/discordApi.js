import request from "./axiosConfig";
import { API_URL, BASE_API_URL } from "../constants/constants";

class DiscordApi {
  discordVerification() {
    return request.get(`${BASE_API_URL}/verify-discord`);
  }

  registerGuildId(tournamentId, guildId) {
    return request.post(
      `${API_URL}/discord/invite-bot/tournament/${tournamentId}?guildId=${guildId}`
    );
  }

  getDiscordServerMembers(tournamentId) {
    return request.get(`${API_URL}/discord/members/tournament/${tournamentId}`);
  }

  updateTournamentDiscordData(tournamentId, body, url) {
    return request.patch(`${API_URL}/discord/${url}/tournament/${tournamentId}`, body);
  }
}

export const discordApi = new DiscordApi();
