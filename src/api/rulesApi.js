import request from "./axiosConfig";
import { API_URL } from "../constants/constants";

class RulesApi {
  changeRules(tournamentId, body) {
    return request.put(`${API_URL}/rules/change/${tournamentId}`, body);
  }
}

export const rulesApi = new RulesApi();
