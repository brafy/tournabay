import request from "./axiosConfig";
import { API_URL } from "../constants/constants";

class TournamentApi {
  getTournaments() {
    return request.get(`${API_URL}/tournaments`);
  }

  myTournaments() {
    return request.get(`${API_URL}/my-tournaments`);
  }

  bbCode(tournamentId) {
    return request.get(`${API_URL}/tournament/${tournamentId}/bbcode`);
  }

  createTournament(body) {
    return request.post(`${API_URL}/tournament/create`, body);
  }

  getTournamentById(id) {
    return request.get(`${API_URL}/tournament/${id}`);
  }

  register(tournamentId, body) {
    return request.put(`${API_URL}/tournament/${tournamentId}/register`, body);
  }
}

export const tournamentApi = new TournamentApi();
