import request from "./axiosConfig";
import { API_URL } from "../constants/constants";

class TournamentSettingsApi {
  updateTournamentSettings(tournamentId, body) {
    return request.patch(`${API_URL}/tournament/${tournamentId}/settings`, body);
  }
}

export const tournamentSettingsApi = new TournamentSettingsApi();
